import React, { Component } from 'react';
import './App.scss';
import Events from './components/Events'

class App extends Component {
  render() {
    return (
      <Events />
    );
  }
}

export default App;
