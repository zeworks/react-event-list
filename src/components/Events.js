import React, { Component } from 'react'
import { Grid, Row, Col, Modal, Button, Alert } from 'react-bootstrap'
// components
import NewEvent from './NewEvent'
import ListEvents from './ListEvents'

export default class Events extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            event: [],
            checkedEvents: [],
            randomEvent: 0,
            toggleModal: false,
            showError: false
        }

        this.startChoose = this.startChoose.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.randomEvent = this.randomEvent.bind(this);
    }

    onAddEvent(newEvent){
        var joinEvent = this.state.event.concat(newEvent)

        this.setState({
            event: joinEvent
        })
    }

    RemoveEvent(index){
        const { event, checkedEvents } = this.state

        event.splice(index, 1)
        checkedEvents.splice(index, 1)
        this.setState({
            event,
            checkedEvents,
        })
    }

    SelectEvent(event){
        this.setState({
            checkedEvents: this.state.checkedEvents.concat(event)
        });
    }

    unSelectEvent(index){
        const { checkedEvents } = this.state

        checkedEvents.splice(index, 1)
        this.setState({
            checkedEvents
        })
    }

    startChoose(){
        const { checkedEvents } = this.state;

        if(checkedEvents.length > 0) {
            this.toggleModal();
            this.randomEvent();
        } else {
            this.setState({
                showError: true
            }, () => {
                setTimeout(() => {
                    this.setState({ showError: false })
                }, 3000);
            });
        }
    }

    randomEvent(){
        this.setState({
            randomEvent: this.state.checkedEvents[Math.floor(Math.random() * this.state.checkedEvents.length)]
        })
    }

    toggleModal(){
        this.setState({
            toggleModal: !this.state.toggleModal
        })
    }
    
    render() {
        const { event, randomEvent, showError } = this.state

        return (
            <Grid>
                <Row>
                    <h1>EVENTS: [this is the parent component]</h1>
                    <hr style={ { borderColor: '#000' } } />
                    {
                        showError && 
                        <Alert bsStyle="danger">
                            <strong>Alert!</strong> To choose a random event, you need to select events first!
                        </Alert>
                    }
                    {/* add new event */}
                    <Col xs={12} sm={4}>
                        <NewEvent 
                            addEvent={this.onAddEvent.bind(this)} />
                    </Col>
                    {/* events list */}
                    <Col xs={12} sm={4}>
                        <ListEvents 
                            events={event} 
                            remove={this.RemoveEvent.bind(this)} 
                            select={this.SelectEvent.bind(this)} 
                            unselect={this.unSelectEvent.bind(this)}/>
                        <br/>
                        {
                            event.length > 0 &&
                            <button onClick={this.startChoose}>Start Choose</button>
                        }
                    </Col>
                </Row>
                { 
                    this.state.toggleModal &&
                    <div className="static-modal">
                        <Modal.Dialog>
                            <Modal.Header>
                                <Modal.Title>Random Place</Modal.Title>
                            </Modal.Header>
                            <Modal.Body><strong>Result: </strong>{randomEvent}</Modal.Body>
                            <Modal.Footer>
                                <Button bsStyle="primary" onClick={this.randomEvent}>Try Again!</Button>
                                <Button onClick={this.toggleModal}>Close</Button>
                            </Modal.Footer>
                        </Modal.Dialog>
                    </div>
                }
            </Grid>
        )
    }
}
