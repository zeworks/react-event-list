import React, { Component } from 'react'

export default class ListEvents extends Component {

  constructor(props) {
      super(props)
    
      this.RemoveEvent = this.RemoveEvent.bind(this)
      this.CheckEvent = this.CheckEvent.bind(this)
  }

  RemoveEvent(index){
      this.props.remove(index)
  }

  CheckEvent(event){
      this.props.select(event)
  }

  unCheckEvent(index){
      this.props.unselect(index)
  }

  render() {
    const { events } = this.props
    const _this = this

    return (
      <div>
        <h6>this is a child component</h6>
        <h2>Event list</h2>
        <table style={{width: "100%"}}>
          <thead>
            <tr>
              <th>{/* select box header */}</th>
              <th>ID</th>
              <th>Event Name</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          {
           events.map(function(event, index){
              return (
                <tr key={index}>
                  <td>
                    <input type="checkbox" 
                    onClick={(e) => { e.target.checked === true ? _this.CheckEvent(event) : _this.unCheckEvent(index) } }/>
                  </td>
                  <td>
                    {index}
                  </td>
                  <td>
                    {event}
                  </td>
                  <td>
                    <button onClick={() => _this.RemoveEvent(index)}>remove event</button>
                  </td>
                </tr>
              )
            })
          }
          </tbody>
        </table>
      </div>
    )
  }
}
