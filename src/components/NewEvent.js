import React, { Component } from 'react'

export default class NewEvent extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
        event: ''
      }
      this.handleChange = this.handleChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e){
        this.setState({ event: e.target.value })
    }

    handleSubmit(e){
        e.preventDefault();

        this.props.addEvent(this.state.event)
        this.setState({ event: '' })
    }

    render() {
        const { event } = this.state;
        
        return (
            <form onSubmit={this.handleSubmit}>
                <h6>this is a child component</h6>
                <h2>Add new event</h2>
                <input type="text" value={event} onChange={this.handleChange} />
            </form>
        )
  }
}
